module.exports = {
    root: true,
    extends: ['standard'],
    globals: {
        IS_DEVELOPMENT: 'readonly'
    },
    parserOptions: {
        ecmaVersion: 2021
    },
    rules: {
        indent: ['error', 4],
        quotes: ['error', 'single'],
        semi: ['error', 'never']
    }
}
